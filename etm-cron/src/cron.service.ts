import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { TransactionService } from './entity/transaction/transaction.service';

@Injectable()
export class CronService {
  constructor(private readonly transactionService: TransactionService) {}
  @Cron(CronExpression.EVERY_MINUTE)
  async handleCron() {
    await this.transactionService.fetchAndSaveTransactions();
  }
}
