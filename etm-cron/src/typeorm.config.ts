import { etherscanConfig } from 'config';
import * as path from 'path';
import { DataSource } from 'typeorm';

const config = new DataSource({
  type: 'postgres',
  host: etherscanConfig.db.host,
  port: etherscanConfig.db.port,
  username: etherscanConfig.db.username,
  password: etherscanConfig.db.password,
  database: etherscanConfig.db.database,
  entities: [path.resolve(`${__dirname}/../../**/**.entity{.ts,.js}`)],
  migrations: ['dist/src/migrations/*.{ts,js}'],
  migrationsTableName: 'typeorm_migrations',
  logging: true,
  synchronize: false,
});
export default config;
