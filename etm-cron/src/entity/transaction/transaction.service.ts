import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import axios, { AxiosInstance } from 'axios';
import { etherscanConfig } from 'config';
import { Repository } from 'typeorm';
import { Transaction } from './transaction.entity';

@Injectable()
export class TransactionService {
  private readonly etherscanApi: AxiosInstance;

  constructor(
    @InjectRepository(Transaction)
    private transactionRepository: Repository<Transaction>,
  ) {
    this.etherscanApi = axios.create({
      baseURL: 'https://api.etherscan.io/api',
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }

  async fetchAndSaveTransactions(): Promise<void> {
    try {
      const latestBlock = await this.getLatestBlockNumber();
      const startBlock = etherscanConfig.startBlock; // начальный блок

      const blocksPerRequest = 5;

      for (
        let blockNumber = startBlock;
        blockNumber <= latestBlock;
        blockNumber += blocksPerRequest
      ) {
        const endBlock = Math.min(
          blockNumber + blocksPerRequest - 1,
          latestBlock,
        );
        const blockPromises = [];

        for (
          let currentBlock = blockNumber;
          currentBlock <= endBlock;
          currentBlock++
        ) {
          blockPromises.push(this.getBlockTransactions(currentBlock));
        }

        const blockDatas = await Promise.all(blockPromises);

        for (const blockData of blockDatas) {
          if (blockData && blockData.transactions) {
            for (const tx of blockData.transactions) {
              const transaction = new Transaction();
              transaction.from_address = tx.from;
              transaction.to_address = tx.to;
              transaction.value = tx.value.substring(2);
              transaction.block = this.hexStringToDecimal(tx.blockNumber);
              transaction.timestamp = new Date();

              await this.transactionRepository.save(transaction);
            }
          } else {
            console.log('blockData', blockData);
          }
        }
        // Ждем 1 секунду перед отправкой следующей пачки запросов
        await this.delay(1000);
      }

      console.log(`Saved transactions up to block ${latestBlock}`);
    } catch (error) {
      console.log(`Error fetching and saving transactions: ${error.message}`);
    }
  }

  async delay(ms: number): Promise<void> {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  async getLatestBlockNumber(): Promise<number> {
    const response = await this.etherscanApi.get(
      'https://api.etherscan.io/api?module=proxy&action=eth_blockNumber',
    );
    return parseInt(response.data.result, 16);
  }

  async getBlockTransactions(blockNumber: number): Promise<any> {
    const response = await this.etherscanApi.get(
      `https://api.etherscan.io/api?module=proxy&action=eth_getBlockByNumber&tag=0x${blockNumber.toString(
        16,
      )}&boolean=true&apiKey=${etherscanConfig.apiKey}`,
    );

    return response.data.result;
  }

  hexStringToDecimal(hexString: string): number {
    // Убираем префикс "0x", если он есть
    const sanitizedHexString = hexString.startsWith('0x')
      ? hexString.slice(2)
      : hexString;

    // Парсим строку как шестнадцатеричное число и возвращаем результат
    return parseInt(sanitizedHexString, 16);
  }
}
