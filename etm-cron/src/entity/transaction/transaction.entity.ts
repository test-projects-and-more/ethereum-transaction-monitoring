import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Transaction {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  from_address: string;

  @Column({ nullable: true })
  to_address: string;

  @Column()
  value: string;

  @Column()
  block: number;

  @Column({ type: 'timestamp' })
  timestamp: Date;
}
