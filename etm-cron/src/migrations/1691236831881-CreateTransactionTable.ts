import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateTransactionTable1691236831881 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "transaction" ("id" SERIAL NOT NULL, "from_address" character varying NOT NULL, "to_address" character varying, "value" character varying NOT NULL,"block" character varying NOT NULL, "timestamp" TIMESTAMP NOT NULL, CONSTRAINT "PK_89eadb93a89810556e1cbcd6ab9" PRIMARY KEY ("id"))`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "transaction"`);
  }
}
