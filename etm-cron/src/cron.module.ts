import { Module } from '@nestjs/common';
import { CronService } from './cron.service';
import { TransactionModule } from './entity/transaction/transaction.module';

@Module({
  imports: [TransactionModule],
  providers: [CronService],
})
export class CronModule {}
