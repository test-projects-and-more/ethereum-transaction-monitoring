# Ethereum Transaction Analyzer

This project is designed to fetch Ethereum blockchain transaction data using the etherscan.io API, save it to a PostgreSQL database, and provide an API endpoint to query for addresses with the most significant balance changes over the last 100 blocks. The project is built using TypeScript, Nest.js, and TypeORM.

## Features

- Fetch and store Ethereum blockchain transaction data in a PostgreSQL database.
- Analyze the transactions and find addresses with the most significant balance changes over the last 100 blocks.
- Provides a user-friendly API endpoint to retrieve the results.

## Installation

1. Clone the repository:

2. Navigate to the project directory:

```bash
cd etm-api
```

or

```bash
cd etm-cron
```

3. Install dependencies for both the cron-job service and the API service:

```bash
npm install
```

4. Set up your `PostgreSQL database and other properties` and update the connection settings in `etm-api\config.ts` AND `etm-cron\config.ts`
   In any of the services run the command Run migrations to initialize the database:

```bash
cd etm-cron
npm run migrate
```

or

```bash
cd etm-api
npm run migrate
```

# Usage

## Start the Cron-Job Service

The Cron-Job service fetches Ethereum transaction data and saves it to the database every minute, starting from block number 17583000. `(However, the block number can be changed in the config)`

```bash
cd etm-cron
npm run start
```

## Start the API Service

The API service provides an endpoint to retrieve the address with the most significant balance changes over the `last 100 blocks`.

```bash
cd etm-api
npm run start
```

## API Endpoint

Retrieve the address with the most significant balance change over the last 100 blocks:

```bash
GET http://localhost:3000/transaction/largest-change
```

## Technologies Used

- TypeScript
- Nest.js
- TypeORM
- PostgreSQL

## Contributions

Contributions are welcome! If you find any issues or want to improve the project, feel free to submit a pull request.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
