import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Transaction } from './transaction.entity';

@Injectable()
export class TransactionService {
  constructor(
    @InjectRepository(Transaction)
    private transactionRepository: Repository<Transaction>,
  ) {}

  async findAddressWithMaxBalanceChange() {
    const transactionRepository = this.transactionRepository;
    const entityManager = transactionRepository.manager;

    const targetBlock = await entityManager
      .createQueryBuilder()
      .select('MAX(t.block) - 100', 'target_block')
      .from(Transaction, 't')
      .getRawOne();

    const fromSubquery = entityManager
      .createQueryBuilder()
      .select('t.from_address', 'address')
      .addSelect(
        `SUM(CASE WHEN position('0x' in t.value) = 1 THEN ('x' || substring(t.value, 3))::bit(256)::bit(64)::bigint ELSE ('x' || t.value)::bit(256)::bit(64)::bigint END)`,
        'total_sent',
      )
      .addSelect('0', 'total_received')
      .from(Transaction, 't')
      .where(`t.block >= (${targetBlock.target_block})`)
      .groupBy('t.from_address');

    const toSubquery = entityManager
      .createQueryBuilder()
      .select('t.to_address', 'address')
      .addSelect('0', 'total_sent')
      .addSelect(
        `SUM(CASE WHEN position('0x' in t.value) = 1 THEN ('x' || substring(t.value, 3))::bit(256)::bit(64)::bigint ELSE ('x' || t.value)::bit(256)::bit(64)::bigint END)`,
        'total_received',
      )
      .from(Transaction, 't')
      .where(`t.block >= (${targetBlock.target_block})`)
      .groupBy('t.to_address');

    const query1 = fromSubquery.getQuery();
    const query2 = toSubquery.getQuery();

    const blockBalancesQuery = query1 + ' UNION ALL ' + query2;

    const finalQuery = `
    WITH BlockBalances AS (${blockBalancesQuery})
    SELECT address, SUM(total_sent) - SUM(total_received) AS "balance_change"
    FROM BlockBalances
    GROUP BY address
    ORDER BY ABS(SUM(total_sent) - SUM(total_received)) DESC
    LIMIT 1`;

    return await this.transactionRepository.query(finalQuery);
  }
}
