import { Injectable } from '@nestjs/common/decorators';
import { TypeOrmModuleOptions, TypeOrmOptionsFactory } from '@nestjs/typeorm';
import { etherscanConfig } from 'config';

@Injectable()
export class TypeOrmConfigService implements TypeOrmOptionsFactory {
  public createTypeOrmOptions(): TypeOrmModuleOptions {
    return {
      type: 'postgres',
      host: etherscanConfig.db.host,
      port: etherscanConfig.db.port,
      database: etherscanConfig.db.database,
      username: etherscanConfig.db.username,
      password: etherscanConfig.db.password,
      autoLoadEntities: true,
      migrations: ['dist/src/migrations/*.{ts,js}'],
      migrationsTableName: 'typeorm_migrations',
      logger: 'file',
      synchronize: true, // never use TRUE in production!
    };
  }
}
