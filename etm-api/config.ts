export const etherscanConfig = {
  // Данные для подключения к БД
  db: {
    host: 'localhost',
    port: 5432,
    database: 'etm',
    username: 'postgres',
    password: 'postgres',
  },
};
